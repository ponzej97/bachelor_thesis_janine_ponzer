var environment_metabolites;

eel.expose(start_gui);
function start_gui(metabolite_dict) {
    environment_metabolites = metabolite_dict;
    create_selection_box();
}

function create_selection_box() {
    var str = '<h3>Environment</h3><h5>Select metabolites as input for the network:</h5>'
    str += '<form id="selection-form">';
    for (var id in environment_metabolites) {
        str += '<div class="select_btn"><input type="checkbox" id="' + id +'" name="metabolite">'
            + '<label for="metabolite">' + environment_metabolites[id] + '</label></div><br>'
    }
    str += '<button type="button" onclick="run_selection()">Start</button><br>';
    str += '<button type="button" onclick="create_comparison_view()">Compare</button>';
    str += '<button type="button" onclick="select_all_metabolites()">Select all</button><br>';
    str += '<button type="button" onclick="clear_selection()">Clear selection</button>';
    str += '</form>';
    document.getElementById('selection-box').innerHTML += str;
}

function get_checkbox_values() {
    var checkboxes = document.getElementsByName('metabolite');
    var cb_metabolites = Array();
    for (var cb of checkboxes) {
        if (cb.checked) {
            cb_metabolites.push(cb.id)
        }
    }
    return cb_metabolites
}

function run_selection() {
    if (document.getElementById('comparison-selection')) {
        document.getElementById('comparison-selection').remove();
    }
    if (document.getElementById('comparison-result')) {
        document.getElementById('comparison-result').remove();
    }
    document.getElementById('growth-table-view').innerHTML = '<div class="loader"></div>';
    var selected_metabolites = get_checkbox_values();
    eel.create_table_view(selected_metabolites);
}

function select_all_metabolites() {
    var checkboxes = document.getElementsByName('metabolite');
    for (var cb of checkboxes) {
        cb.checked = true;
    }
}

function clear_selection() {
    var checkboxes = document.getElementsByName('metabolite');
    for (var cb of checkboxes) {
        cb.checked = false;
    }
}

eel.expose(create_view_box);
function create_view_box(efm_growth_dict, ordered_efms) {
    var efm_ids = Object.keys(efm_growth_dict);
    var str = '';
    if (Object.keys(efm_growth_dict).length === 0) {
        str += '<div id="show-efm-amount">We found <b>' + efm_ids.length + '</b> EFMs.</div>';
    } else {
        str += '<div id="show-efm-amount">We found <b>' + efm_ids.length + '</b> EFMs:</div>';
        var cols = Object.keys(efm_growth_dict[efm_ids[0]]).length;
        str += '<div class="scroll_table"><table><tr><th class="efm_id">EFM#</th><th colspan="' + cols + '">Elementary flux mode</th></tr>';
        for (var id of ordered_efms) {
            str += '<tr><td class="efm_id">' + id + '</td>';
            for (var met in efm_growth_dict[id]) {
                n = efm_growth_dict[id][met].toFixed(3);
                str += '<td>' + (n != 0? ((n < 0? "": "+") + n + ' ' + met): "") + '</td>';
            }
            str += '</tr>';
        }
        str += '</table></div>';
    }
    document.getElementById('growth-table-view').innerHTML = str;
}

eel.expose(create_rg_view);
function create_rg_view(regulatory_genes_dict) {
    var str = "";
    for (var id in regulatory_genes_dict) {
        var set_class = "";
        if (regulatory_genes_dict[id]) {
            set_class = "rg_active";
        } else {
            set_class = "rg_repressed";
        }
        str += '<div class="' + set_class + '">' + id + '</div>';
    }
    document.getElementById('regulatory-genes-view').innerHTML = str;
}

function change_value(id) {
    var btn = document.getElementById(id);
    if (btn.classList.contains('met_on')) {
        btn.classList.remove('met_on');
        btn.classList.add('met_off');
    } else {
        btn.classList.remove('met_off');
        btn.classList.add('met_on');
    }
}

function create_comparison_view() {
    var selected_metabolites = get_checkbox_values();
    var str = '<div id="comparison-selection"><div id="environments-to-compare">';
    str += '<div id="fst-row" class="metabolites_to_compare">';
    str += '<h4>1st environment:</h4>';
    for (var id in environment_metabolites) {
        var set_class = '';
        if (selected_metabolites.includes(id)) {
            set_class = "met_on";
        } else {
            set_class = "met_off";
        }
        str += '<button type="button" id="met-1-' + id + '" class="' + set_class + '" onclick="change_value(this.id)">' + id + '</button>';
    }
    str += '</div><br>';
    str += '<div id="snd-row" class="metabolites_to_compare">';
    str += '<h4>2nd environment:</h4>';
    for (var id in environment_metabolites) {
        str += '<button type="button" id="met-2-' + id + '" class="met_off" onclick="change_value(this.id)">' + id + '</button>';
    }
    str += '</div></div>';
    str += '<div id="btn-div"><button type="button" id="compare-btn" onclick="compare_environments()">Compare environments</button></div></div>';
    document.getElementById('regulatory-genes-view').innerHTML = '';
    document.getElementById('growth-table-view').innerHTML = '';
    if (document.getElementById('comparison-selection')) {
        document.getElementById('comparison-selection').remove();
    }
    if (document.getElementById('comparison-result')) {
        document.getElementById('comparison-result').remove();
    }
    document.getElementById('view-box').innerHTML += str;
    clear_selection()
}

function compare_environments() {
    if (document.getElementById('comparison-result')) {
        document.getElementById('comparison-result').remove();
    }
    var str = '<div id="comparison-result">';
    var met_buttons = document.querySelectorAll('.metabolites_to_compare button');
    var list_of_lists = Array()
    for (var k = 1; k < 3; k++) {
        var list_of_active_metabolites = [k] // erstes Element ist die Nummerierung
        for (var i = 0; i < met_buttons.length; i++) {
            var metabolite = met_buttons[i];
            if (metabolite.classList.contains('met_on')) {
                var id = metabolite.id;
                var numbering = parseInt(id.substring(4, 5));
                if (k == numbering) {
                    list_of_active_metabolites.push(id.substring(6));
                }
            }
        }
        list_of_active_metabolites.shift();
        list_of_lists.push(list_of_active_metabolites)
    }
    eel.getPlot(list_of_lists);
    str += '<img id="venn-graphic" src=""/>';
    document.getElementById('view-box').innerHTML += str;
    var timestamp = new Date().getTime();
    var img = document.getElementById('venn-graphic');
    img.src = "venn.png?t=" + timestamp;
}

function show_efms(id) {
    var id_number = id.substring(4);
    var efm_list = document.getElementById("lst-" + id_number);
    if (efm_list.classList.contains("lists-hide")) {
        efm_list.classList.remove("lists-hide");
        efm_list.classList.add("lists-show");
    } else {
        efm_list.classList.remove("lists-show");
        efm_list.classList.add("lists-hide");
    }
}

eel.expose(create_result_info);
function create_result_info(efms_A, efms_B, intersection, difference_a_wo_b, difference_b_wo_a, sym_diff) {
    var str = '<div id="result-info">';
    str += '<div id="amt-1" class="amounts" onclick="show_efms(this.id)">EFMs in the 1st environment:<a>' + efms_A.length + '</a></div>';
    str += '<div id="lst-1" class="lists lists-hide">' + String(efms_A).replaceAll(",", ", ") + '</div>';
    str += '<div id="amt-2" class="amounts" onclick="show_efms(this.id)">EFMs in the 2nd environment:<a>' + efms_B.length + '</a></div>';
    str += '<div id="lst-2" class="lists lists-hide">' + String(efms_B).replaceAll(",", ", ") + '</div>';
    str += '<div id="amt-3" class="amounts" onclick="show_efms(this.id)">EFMs in the intersection:<a>' + intersection.length + '</a></div>';
    str += '<div id="lst-3" class="lists lists-hide">' + String(intersection).replaceAll(",", ", ") + '</div>';
    str += '<div id="amt-4" class="amounts" onclick="show_efms(this.id)">EFMs in the symmetrical difference:<a>' + sym_diff.length + '</a></div>';
    str += '<div id="lst-4" class="lists lists-hide">' + String(sym_diff).replaceAll(",", ", ") + '</div>';
    str += '<div id="amt-5" class="amounts" onclick="show_efms(this.id)">EFms in the 1st without the 2nd environment:<a>' + difference_a_wo_b.length + '</a></div>';
    str += '<div id="lst-5" class="lists lists-hide">' + String(difference_a_wo_b).replaceAll(",", ", ") + '</div>';
    str += '<div id="amt-6" class="amounts" onclick="show_efms(this.id)">EFMs in the 2nd without the 1st environment:<a>' + difference_b_wo_a.length + '</a></div>';
    str += '<div id="lst-6" class="lists lists-hide">' + String(difference_b_wo_a).replaceAll(",", ", ") + '</div>';
    str += '</div>';
    document.getElementById('comparison-result').innerHTML += str;
}