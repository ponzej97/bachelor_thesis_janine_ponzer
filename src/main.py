#!/usr/bin/env python3

"""ecoli_core.py: Main Program"""

__author__ = "Janine Ponzer"

from definitions import *
from pathlib import Path
from cobra.io import load_model, load_matlab_model
from multiprocessing import Pool, cpu_count
from itertools import repeat
import time
import csv
import eel
import matplotlib.pyplot as plt
from matplotlib_venn import venn2, venn2_circles

# load E. coli core model from matlab file
model_path = Path('.') / '..' / 'bachelor_thesis_janine_ponzer' / 'data' / 'ecoli_core_model.mat'
model = load_matlab_model(f'{model_path.resolve()}')

# read regulatory rules from excel file and store as pandas dataframe
rules_path = Path('.') / '..' / 'bachelor_thesis_janine_ponzer' / 'data' / 'core_regulatory_rules.xls'
rules = create_regulatory_rules_df(f'{rules_path.resolve()}')

environment_metabolites = get_metabolite_inputs(rules)
transport_reactions = get_transport_reactions(model, environment_metabolites)


try: # files should exist after one complete run of the program
    with open('result_environments.csv') as result_environments:
        results_df = pd.read_csv(result_environments, sep=';', encoding='utf-8')
    with open('efms.csv') as calculated_efms:
        efms_df = pd.read_csv(calculated_efms, sep=';', encoding='utf-8')

except IOError: # executed if result_environments.csv or efms.csv does not exist yet
    reaction_ids = [r.id for r in model.reactions]
    print('EFMs are being calculated...')
    efms = get_efms(model, reaction_ids)
    print(f'EFMTool found {efms.shape[1]} EFMs.')

    efms_dict = create_efms_dict(efms, reaction_ids)
    efms_df = save_efms_as_csv(efms_dict)

    regulation_dict = get_regulation_dict(model, rules, reaction_ids, environment_metabolites)
    environments = create_environment_variations(environment_metabolites)

    start = time.time()
    extracted_results = None

    # parallel processing of check_pathway_feasibility()
    with Pool(4) as pool:
        results = pool.starmap(check_pathway_feasibility, 
                                    zip(environments, 
                                    repeat(efms_dict), 
                                    repeat(regulation_dict),
                                    repeat(transport_reactions)))
        pool.close()
        pool.join()

        time_in_sec = time.gmtime(time.time() - start)
        print(time.strftime("%H:%M:%S", time_in_sec))

        extracted_results = results

    print('Number of environments:', len(extracted_results)) # should be 2048

    # result export
    fileheader = []
    for metabolite in environment_metabolites:
        fileheader.append(metabolite)
    for regulator in extracted_results[0][2]:
        fileheader.append(regulator)
    fileheader.append('pathways')
    fileheader.append('pathway_list')

    f = open('result_environments.csv', 'w', encoding='UTF8')
    writer = csv.writer(f, delimiter=';')
    writer.writerow(fileheader)

    for res in extracted_results:
        env = res[0] # dict
        pws = res[1] # list
        reg = res[2] # dict

        row = []
        for met_val in env.values():
            row.append(met_val)
        for reg_val in reg.values():
            row.append(int(reg_val))
        row.append(len(pws))
        row.append(pws)
        writer.writerow(row)
    
    results_df = pd.read_csv('result_environments.csv', sep=";", encoding='utf-8')

# rank list for efm frequencies
list_of_efm_lists = []
for pw_list in results_df['pathway_list']:
    list_of_efm_lists += eval(pw_list)
efm_count_df = pd.DataFrame(list_of_efm_lists, columns=['efm_id']).value_counts().reset_index(name='rank')
efm_count_df.to_csv('efm_count.csv', sep=';', encoding='utf-8')


#### GUI ####

# quit program if gui window is closed
def close_callback(route, websockets):
    if not websockets:
        exit()

# look for all possible extracellular metabolites after transport reactions
def environment_to_table(efm_list):
    if not efm_list:
        eel.create_view_box({})
    else:
        view_table_df = pd.DataFrame()
        # fill table with transport reaction fluxes of all efms in the list
        for efm_number in efm_list:
            efm = efms_df.loc[efms_df['id'] == efm_number].iloc[0]
            col_names = ['EFM#'] + environment_metabolites + ['Biomass']
            col_values = [efm_number]
            for met in environment_metabolites:
                col_values.append(efm[transport_reactions[met]])
            col_values.append(efm['Biomass_Ecoli_core_w_GAM'])
            df_row = [col_values]
            res_df = pd.DataFrame(df_row, columns=col_names)
            view_table_df = pd.concat([view_table_df, res_df], ignore_index=True)

        # Columns rearrangement (negative fluxes to the right)
        left_table = []
        right_table = []
        for col in view_table_df:
            if col == 'EFM#':
                left_table.append(col)
            elif (view_table_df.loc[:,col] < 0).sum().sum() > 0 or col == 'Biomass':
                right_table.append(col)
            elif col != 'o2[e]':
                left_table.append(col)
        new_col_order = left_table + ['o2[e]'] + right_table
        view_table_df = view_table_df[new_col_order]
        if 'Biomass' in view_table_df.columns:
            view_table_df = view_table_df.sort_values(by=['Biomass'], ascending=False)
        view_table_df = view_table_df.loc[:, (view_table_df != 0).any(axis=0)]
        # print(view_table_df)

        order_list = list(view_table_df['EFM#'])
        view_table_dict = view_table_df.set_index('EFM#').T.to_dict()
        eel.create_view_box(view_table_dict, order_list)

# search in row of result that corresponds to the selected environment
def environment_to_table_row(checked_metabolites):
    # create dictionary corresponding to the ON/OFF values of the environment selection
    env_dict = dict([(met, 0) for met in environment_metabolites])
    for id in checked_metabolites:
        env_dict[id] = 1
    # create query to find the environment row corresponding to the metabolite values
    query_str = ''.join(f'(`{key}` == "{value}") and ' for key, value in env_dict.items())
    query_str = query_str.rstrip('and ')
    env_row = results_df.astype(str).query(query_str) # searched row
    res = env_row.iloc[0] # row content
    return res

# obtain efms that are found in the selected environment
def get_pathway_list(checked_metabolites):
    res = environment_to_table_row(checked_metabolites)
    efm_list = eval(res['pathway_list'])
    return efm_list

# calculate set properties for both selected environments and create venn diagram
@eel.expose
def getPlot(list_of_environment_lists):
    pathway_lists = []
    for environment in list_of_environment_lists:
        pw_list = get_pathway_list(environment)
        pathway_lists.append(pw_list)

    pathway_list_sets = [set(lst) for lst in pathway_lists]
    intersection = list(pathway_list_sets[0].intersection(pathway_list_sets[1]))
    difference_a_wo_b = list(pathway_list_sets[0].difference(pathway_list_sets[1]))
    difference_b_wo_a = list(pathway_list_sets[1].difference(pathway_list_sets[0]))
    sym_diff = difference_a_wo_b + difference_b_wo_a
    sym_diff.sort()

    d1 = pathway_list_sets[0]
    d2 = pathway_list_sets[1]

    plt.figure()
    venn2([set(d1), set(d2)], set_labels = ('1st', '2nd'))
    plt.title('Venn diagram of the selected environments:')
    plt.savefig("src/web/venn.png")

    eel.create_result_info(pathway_lists[0], pathway_lists[1], intersection, difference_a_wo_b, difference_b_wo_a, sym_diff)

# retrieve information for gui main view (efm table)
@eel.expose
def create_table_view(selected_metabolites):
    res = environment_to_table_row(selected_metabolites)
    pw_list = eval(res['pathway_list'])
    # create dictionary of regulators in the row and their boolean values 
    rg_dict = dict([(rg, int(eval(res[rg]))) for rg in get_regulatory_genes(rules)])
    eel.create_rg_view(rg_dict)
    environment_to_table(pw_list)

# start gui application
eel.init('src/web')

# information on the metabolites full names
metabolite_dict = dict([(met_id, model.metabolites.get_by_id(met_id).name) for met_id in environment_metabolites])
eel.start_gui(metabolite_dict)

eel.start('main.html', mode='firefox', close_callback=close_callback)