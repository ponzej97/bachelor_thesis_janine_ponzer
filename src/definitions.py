#!/usr/bin/env python3

"""definitions.py: Function and variable definitions for working with Sample Metabolic Network"""

__author__ = "Janine Ponzer"

from cobra import Model, Reaction, Metabolite
from cobra.util.array import create_stoichiometric_matrix
from itertools import product
import efmtool
import numpy as np
import pandas as pd


def create_regulatory_rules_df(path: str) -> pd.DataFrame:
    """
    Pass the information from the excel file into a pandas dataframe

    Returns:
        pd.DataFrame: pandas dataframe with column names bnum, gene, rule, type
    """

    # read 2nd sheet of excel file starting line 3
    df = pd.read_excel(path, sheet_name=1, header=2)

    # format data
    df.columns = map(str.lower, df.columns)
    last = df.columns.values.tolist().pop()
    df = df.rename({last: 'type'}, axis=1)
    df['type'] = df['type'].fillna(method='ffill') # forward fill
    
    return df


def get_efms(model: Model, reaction_ids: list[str]) -> np.ndarray:
    """
    Calculate the efms of the passed cobra model using the efmtool

    Returns:
        np.ndarray: efm values as a multidimensional array
    """

    # define parameters for efm calculation
    stoichiometry = create_stoichiometric_matrix(model)
    reversibilities = [int(r.reversibility) for r in model.reactions]
    metabolite_ids = [m.id for m in model.metabolites]
    
    efms = efmtool.calculate_efms(stoichiometry, reversibilities, reaction_ids, metabolite_ids)

    return efms


def create_efms_dict(efms: np.ndarray, reaction_ids: list[str]) -> dict[int, dict[str, tuple[float, int]]]:
    """
    Create dictionary to store numbered efms linking the efm values to their corresponding reaction ids

    Returns:
        dict[int, dict[str, tuple[float, int]]]: numbered dictionary of efm dictionaries with reaction-flux-pairs
    """

    efms_dict = {}

    for k, efm in enumerate(np.transpose(efms)):

        efm_tuple_dict = {}

        for r_id, flux in zip(reaction_ids, efm):

            # pseudo value for boolean evaluation
            # negative values might be necessary to determine the flux direction of reversible reactions
            int_val = (1 if flux > 0 else (-1 if flux < 0 else 0))

            efm_tuple_dict[r_id] = (flux, int_val)
            efms_dict[k+1] = efm_tuple_dict #FIXME
    
    return efms_dict


def save_efms_as_csv(efms_dict: dict[int, dict[str, tuple[float, int]]]) -> pd.DataFrame:
    """ Extract information of efms dictionary into dataframe and save as csv file

    Returns:
        pd.DataFrame: dataframe containing the efms with their flux values per reaction
    """

    efms_df = pd.DataFrame()
    col_names = list(efms_dict[1].keys())
    col_names.insert(0, 'id')
    for k, v in efms_dict.items():
        row_vals = [k]
        for val in v.values():
            row_vals.append(val[0])
        df_row = [row_vals]
        df = pd.DataFrame(df_row, columns=col_names)
        efms_df = pd.concat([efms_df, df], ignore_index=True)
    efms_df.to_csv('efms.csv', sep=';', header=col_names, encoding='utf-8', index=False)

    return efms_df


def get_metabolite_inputs(regulatory_rules_df: pd.DataFrame) -> list[str]:
    """
    Read out extracellular metabolites necessary for the evaluation of the regulatory rules
    
    Returns:
        list[str]: list containing environment metabolites
    """

    regulators_df = regulatory_rules_df.loc[regulatory_rules_df['type'] == 'metabolite inputs']
    
    metabolite_list = regulators_df['gene'].tolist()

    return sorted(metabolite_list)


def get_transport_reactions(model: Model, environment_metabolites: list[str]) -> dict[str, str]:
    """
    Assign transport reactions to corresponding environment metabolites
    
    Returns:
        dict[str, str]: dictionary containing metabolite-reaction-pairs
    """

    transport_reactions = {}

    for metabolite in environment_metabolites:
        for rxn in model.metabolites.get_by_id(metabolite).reactions:
            transport_rxn = ''
            if rxn.id[:3] != 'EX_':
                if metabolite != 'succ[e]':
                    transport_rxn = rxn.id
                else:
                    transport_rxn = 'SUCCt2_2'
                transport_reactions[metabolite] = transport_rxn
    
    return transport_reactions


def get_regulatory_genes(regulatory_rules_df: pd.DataFrame) -> list[str]:
    """
    Search names of regulatory genes from table in the given data frame (originally Excel table)

    Returns:
        list[str]: list containg the names of the regulatory genes
    """

    rg_rows = regulatory_rules_df.loc[regulatory_rules_df['type'] == 'regulatory genes and rules']
    rg_list = rg_rows['gene'].to_list()

    return rg_list


def process_regulatory_rule(grr: str, regulatory_rules_df: pd.DataFrame, regulatory_genes: dict[str, bool], environment_metabolites: list[str]) -> str:
    """
    From the passed gene reaction rule, form a logical expression by inserting the regulatory rules for the metabolic and regulatory genes

    Returns:
        str: gene reaction rule as filled out string, readable by eval()
    """

    # string list of gene reaction rule (grr)
    grr_list = grr.replace('(', ' ( ').replace(')', ' ) ').split()
    bool_expr = ''

    for elem in grr_list:

        if elem in ['and', 'or', '(', ')']:
            bool_expr = bool_expr + ' ' + elem + ' '

        # if metabolic gene found
        else:
            gene_rule = regulatory_rules_df.loc[regulatory_rules_df.bnum == elem]['rule'].item()
            gene_rule_list = gene_rule.replace('(', ' ( ').replace(')', ' ) ').split()
            rule = ''

            for elem in gene_rule_list:
                r = ''
                if elem in ['true', 'false']:
                    r = elem.capitalize() + ' '
                else:
                    if elem in ['(', ')']:
                        r = ' ' + elem + ' '
                    elif elem in ['AND', 'NOT', 'OR']:
                        r = ' ' + elem.lower() + ' '
                    else:
                        if elem in regulatory_genes:
                            r = f'regulators["{elem}"] '
                        elif elem in environment_metabolites:
                            r = f'environment["{elem}"] '
                        else:
                            print(elem)
                            breakpoint() # catching errors

                rule = rule + str(r)
            bool_expr = bool_expr + '(' + rule + ') '

    return bool_expr


def get_regulation_dict(model: Model, regulatory_rules_df: pd.DataFrame, reaction_ids: list[str], environment_metabolites: list[str]) -> dict[str, str]:
    """
    Generate dictionary of all reactions with a gene reaction rule and their associated formatted gene reaction rule

    Returns:
        dict[str, str]: dictionary assigning the formatted gene reaction rules to their reaction ids
    """

    regulation_dict = {}

    regulatory_genes = get_regulatory_genes(regulatory_rules_df)

    for r_id in reaction_ids:
        grr = model.reactions.get_by_id(r_id).gene_reaction_rule

        # if gene reaction rule exists for the reaction
        if grr:
            regulation_dict[r_id] = process_regulatory_rule(grr, regulatory_rules_df, regulatory_genes, environment_metabolites)

    return regulation_dict


def create_environment_variations(environment_metabolites: list[str]) -> list[dict[str, bool]]:
    """
    Generate all possible environments in which the EFMs will be checked (2^n environments in total)

    Returns:
        list[dict[str, bool]]: list of environments as dictionaries
    """

    n = len(environment_metabolites)

    # {0*n}x{1*n}
    cartesian_product = list(product(range(2), repeat=n))
    cartesian_product.reverse()

    # Assign n-tuples as representation of ON/OFF to the metabolites to create an environment
    environment_variations = [{environment_metabolites[i]: n_tuple[i] for i in range(n)} for n_tuple in cartesian_product]

    return environment_variations


def evaluate_environment_specificity(environment: dict[str, bool], efm: dict[str, tuple[float, int]], transport_reactions: dict[str, str]) -> bool:
    """
    Check if the passed efm is consistent with the given environment

    Returns:
        bool: true of the efm is feasible in the environment, otherwise false
    """

    eval = 1

    for metabolite in environment:
        # stop if metabolite does not exist but its transport flux is active
        if (not environment[metabolite]) and abs(efm[transport_reactions[metabolite]][1]):
            eval = 0
            break
  
    return eval


def check_pathway_feasibility(environment: dict[str, bool], efms: dict[int, dict[str, tuple[float, int]]], reg_rules: dict[str, str], transport_reactions: dict[str, str]) -> [dict[str, bool], list[int], dict[str, bool]]:
    """
    Set regulatory conditions for the given environment and check for each efm the feasibility under regulation constraints

    Returns:
        dict[str, bool]: passed environment dictionary
        list[int]: list of efm ids that are allowed in the given environment
        dict[str, bool]: determined regulators dictionary
    """

    print(f'checking environment {environment}')

    regulators = {}

    # determine values of the regulatory genes which depend on environment metabolites
    regulators['ArcA'] = not environment['o2[e]']
    regulators['DcuS'] = environment['succ[e]'] or environment['fum[e]'] or environment['mal-L[e]']
    regulators['FadR'] = environment['glc-D[e]'] or (not environment['ac[e]'])
    regulators['Fis'] = True # always true at steady state
    regulators['Fnr'] = not environment['o2[e]']
    regulators['GlcC'] = environment['ac[e]']
    regulators['GlnG'] = not environment['nh4[e]']
    regulators['Mlc'] = not environment['glc-D[e]']
    regulators['PhoR'] = not environment['pi[e]']
    regulators['CRPnoGLC'] = not environment['glc-D[e]']
    regulators['CRPnoGLM'] = not (environment['glc-D[e]'] or environment['mal-L[e]'] or environment['lac-D[e]'])
    regulators['surplusFDP'] = ((not eval(reg_rules['FBP'])) and (not (eval(reg_rules['TKT2']) or eval(reg_rules['TALA']) or eval(reg_rules['PGI'])))) or environment['fru[e]']
    
    # determine values of the regulatory genes which depend on the regulatory genes above
    regulators['Crp'] = regulators['CRPnoGLC']
    regulators['DcuR'] = regulators['DcuS']
    regulators['FruR'] = not regulators['surplusFDP']
    regulators['IclR'] = regulators['FadR']
    regulators['PhoB'] = regulators['PhoR']
    regulators['NRI_low'] = regulators['GlnG']
    regulators['Nac'] = regulators['NRI_low']
    regulators['NRI_hi'] = regulators['NRI_low']
    reg_GLCpts = eval(reg_rules['GLCpts'], {'regulators': regulators})
    reg_PYK = eval(reg_rules['PYK'], {'regulators': regulators})
    reg_SUCCt2_2 = eval(reg_rules['SUCCt2_2'], {'regulators': regulators})
    regulators['surplusPYR'] = (not (eval(reg_rules['ME2']) or eval(reg_rules['ME1']))) and (not (reg_GLCpts or reg_PYK or eval(reg_rules['PFK']) or eval(reg_rules['LDH_D']) or reg_SUCCt2_2))
    regulators['PdhR'] = not regulators['surplusPYR']

    pathway_list = []
    evaluated_reg_rules = {}

    # evaluate all gene reaction rules for the given environment
    for reaction in reg_rules:
        rule = reg_rules[reaction]

        has_flux = eval(rule, {}, {'regulators': regulators, 'environment': environment})
        evaluated_reg_rules[reaction] = has_flux

    # iterate efms and check environment specifity and, if needed, regulatory rules
    for pathway in efms:
        p = efms[pathway]
        environment_specificity = evaluate_environment_specificity(environment, p, transport_reactions)

        # only checked if first check was successfull
        if environment_specificity:
            regulation = 1

            # stops if regulatory constraints are violated
            for reaction, has_flux in evaluated_reg_rules.items():  
                if (not has_flux) and abs(p[reaction][1]):
                    regulation = 0
                    break

            # add pathway to result list if both conditions are met
            if regulation:
                pathway_list.append(pathway)

    return environment, pathway_list, regulators