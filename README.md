# EFMs of the E. coli core metabolic network in a complex medium

A software for finding EFMs that are compatible with the regulatory conditions in a given medium. 


## Files

The folder 'data' contains the following data sources provided by the [Systems Biology Research Group](https://systemsbiology.ucsd.edu/Downloads/E_coli_Core).

+ **core_regulatory_rules.xls**
Excel file with the regulatory rules for the E. coli core model.

+ **ecoli_core_model.mat**
Matlab file that describes the E. coli core model.

The folder 'src' contains the source code files as well as a folder 'web' which contains the HTML, CSS and JavaScript files for the web interface of the GUI.

+ **definitions.py**
Python file that contains the function definitions of the program and is integrated into main.py.

+ **main.py**
Main program that reads in the data sources and determines the regulatory valid EFMs for possible environments. The [EFMTool (Python wrapper) of ETH Zurich](https://pypi.org/project/efmtool/) is used to calculate the EFMs. The subsequent application of the regulatory rules and presentation of the results in the GUI is done by this program. 

The root folder holds the result and intermediate result files of the program.

+ **efm_count.csv**
A csv file with the frequencies with which each EFM was found in all considered environments. Sorted in descending order (most frequently found EFM at the beginning).

+ **efms.csv**
A csv file that assigns an ID to the EFMs calculated by the EFMTool.

+ **np_efms.csv**
EFMs calculated by EFMTool as a numpy array, saved in csv format.

+ **result_environments.csv**
Result data. The csv file lists all evaluated environments; for each environment the validity of the regulatory genes is shown as well as a list of the IDs that were EFMs found in it.

## Getting started

To start the program, the file *main.py* is executed. If all result and intermediate result files are present in the root folder, the GUI is immediately opened in a window of the browser specified in line 212 of *main.py*. Otherwise, the calculation of the EFMs and evaluation based on the regulatory rules is started (takes a long time).

Note that some libraries or programs must be loaded in advance.


## GUI Tasks

### Show the EFMs for a selected environment
Select the medium by clicking on the metabolites to be present. Confirm the selected environment by clicking on the *Start* button. The active regulators in the medium are shown in green in the upper bar, the table contains all found EFMs with their IDs and flux values of the extracellular metabolites as well as biomass. Arranged in descending order of absolute biomass.

### Compare the EFM sets of two environments
Clicking the *Compare* button opens a new view where two different environments can be selected. After confirmation via the *Compare environments* button, the common and non-common EFMs of the selected environments are shown (intersection and symmetric difference of both EFM sets). They are presented as a Venn diagram and the respective set groups are displayed with EFM count. A click on these bars opens a listing of the associated EFMs.

## Thesis Abstract
Elementary flux modes (EFMs) are the minimal reaction pathways in molecular networks at steady state. They serve as a tool in systems biology to investigate cell physiology and behavior. However, not all expressed genes that activate specific reactions are active simultaneously. They depend on regulatory events that are controlled by the available substrates in the cell environment. Covert and Palsson (2003) introduced an approach to tackle this issue. They imposed transcriptional regulation as a constraint on the 80 pathways of a sample network using Boolean logic. This thesis transfers their method to the larger E. coli core metabolic network. Regulatory rules are implemented to eliminate EFMs with inconsistencies in transcriptional regulation. For the E. coli core model, 100274 EFMs were calculated and tested against the regulatory rules in 2048 simulated environments of up to 11 metabolites. Therefore developed
software provides the resulting EFMs for a particular environment to further investigate the obtained sets. Confining the possible pathways through cellular metabolism is a small step that can help understand which biochemical processes are crucial to sustaining living systems.
